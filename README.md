# WebGraphicMathFormulasEditor
The app is written on javascript, css, html with a pinch of jQuery.
With help of this app you can create, modify and erase math formulas and different geometric figures. The short indroduction and 'how to use' you can find in the app, you just need to click on button with question mark.

https://pejcapaso.codeberg.page/mathematical/

![Project Picture](Images/WebGraphicMathFormulasEditor.png)

Future plans:
 - introduce a possibility to undo and redo all taken actions on canvas;
 - finally add some comments;
 - last but not the least item is to develop transformation to latex formulas. Actually, it could be my master project.

All contributions and advices are welcome!
