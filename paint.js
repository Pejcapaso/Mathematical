if (window.addEventListener) {
   window.addEventListener('load', function() { init(); });
}

var canvas, context, currentShape = '';
var lastStampId = '', stampId = '', switcher = '';
var counterWidth = 1, counterHeight = 0;
var numbers = ['null', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
var englishLettersLower = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
var englishLettersUpper = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
var greeceLettersLower = ['alpha', 'beta', 'gamma', 'delta', 'epsilon',  'zeta',
  'eta',  'theta', 'iota',  'kappa',  'lambda',  'mu',  'nu',  'xi', 
  'omikron',  'pi',  'rho',  'sigma',  'tau',  'upsilon',  'phi',  'chi', 
  'psi',  'omega'];
var greeceLettersUpper = ['Alpha', 'Beta', 'Gamma', 'Delta', 'Epsilon',  'Zeta',
  'Eta',  'Theta', 'Iota',  'Kappa',  'Lambda',  'Mu',  'Nu',  'Xi', 
  'Omikron',  'Pi',  'Rho',  'Sigma',  'Tau',  'Upsilon',  'Phi',  'Chi', 
  'Psi',  'Omega'];
var primalOperations = ['plus', 'minus', 'plusAndMinus', 'multiply', 'divide', 'degree', 'root'];
var dependency = ['equal', 'notequal', 'isApproximatelyEqualTo', 'isNotApproximatelyEqualTo', 
'isEqualToOrGreaterThan', 'isGreaterThan', 'isLessThan', 'isLessThanOrEqualTo', 'isSimilar', 'isNotSimilar'];
var pars = ['leftPar', 'rightPar', 'leftCurlyBracket', 'rightCurlyBracket', 'leftShellBracket', 'rightShellBracket'];
var sets = ['emptySet', 'intersectionOf', 'isASubsetLeft', 'isASubsetOfLeft', 'isASubsetOfRight', 'isASubsetRight', 'theSetOf'];
var shape = ['line', 'rectangle', 'triangle', 'circle', 'eraser', 'marker'];
var write = ['sin', 'cos', 'tan', 'arc', 'log', 'ln', 'lim'];
var symbols = ['and', 'forAll', 'infinite', 'integral', 'or', 'percent', 'thereExists', 'not', 'angle',
'thereDoesNotExists','leftArrow', 'rightArrow', 'downArrow', 'upArrow', 'isElementOf', 'isNotElementOf', 'factorial', 'dot'];
var size = [16, 24, 32, 48, 64, 96, 128, 192];
var shiftX = [84, 90, 98, 107, 118, 141, 162, 203];
var shiftY = [69, 77, 83, 94, 109, 136, 163, 216];
var sizeX = 64, sizeY = 64;
var x, y, startX, startY;

function init() {
    canvas = $('#image').get(0);
    context = canvas.getContext('2d');
    image = $('#canvas').get(0);
    firstCanvas = document.createElement('canvas');
    firstContext = firstCanvas.getContext('2d');
    canvas.width  = window.innerWidth - 150;
    canvas.height = window.innerHeight - 135;
    firstCanvas.id = 'firstCanvas';
    firstCanvas.width = canvas.width;
    firstCanvas.height = canvas.height;
    image.appendChild(firstCanvas);
    secondCanvas = document.createElement('canvas');
    secondContext = secondCanvas.getContext('2d');
    secondCanvas.id = 'secondCanvas';
    secondCanvas.width = canvas.width;
    secondCanvas.height = canvas.height;
    image.appendChild(secondCanvas);
   

    secondCanvas.addEventListener('mousemove', function(e){
        x = e.offsetX;
        y = e.offsetY;}, false);

    canvas.addEventListener('mousemove', function(e){
    x = e.offsetX;
    y = e.offsetY;}, false);

    canvas.addEventListener('mousedown', function(e) {

          x = e.offsetX;
          y = e.offsetY;
          context.lineJoin = 'round';
          context.lineCap = 'round';
          context.lineWidth = sizeX / 16;
                
          context.beginPath();
          context.moveTo(x, y);

           if(currentShape == 'eraser'){
                canvas.addEventListener('mousemove', eraser, false);
                x = e.offsetX;
                y = e.offsetY;
                eraser();
              } 
              if(currentShape == 'marker'){
                canvas.addEventListener('mousemove', marker, false);
                x = e.offsetX;
                y = e.offsetY;
                marker();
              }
          }, false);

    

    firstCanvas.addEventListener('mousedown', function(e) {
                
               firstCanvas.addEventListener('mousemove', click, false); 

         }, false);



    secondCanvas.addEventListener('mousedown', function(e) {
                 
                 startX = x;
                 startY = y; 
                secondContext.lineJoin = 'round';
                secondContext.lineCap = 'round';
                secondContext.lineWidth = sizeX / 16;           
                  x = e.offsetX;
                  y = e.offsetY;
                if(currentShape == 'line'){
                secondCanvas.addEventListener('mousemove', line, false);
              }
                 if(currentShape == 'circle'){ 
                secondCanvas.addEventListener('mousemove', circle, false);
              }
               if(currentShape == 'rectangle'){
                secondCanvas.addEventListener('mousemove', rectangle, false);
              }
              if(currentShape == 'triangle'){
                secondCanvas.addEventListener('mousemove', triangle, false);
              }
            }, false);
         
         canvas.addEventListener('mouseup', function() {
           canvas.removeEventListener('mousemove', eraser, false);
           canvas.removeEventListener('mousemove', marker, false);
            
            context.globalCompositeOperation = 'source-over';
        }, false); 

       firstCanvas.addEventListener('mouseup', function() { 
              firstCanvas.removeEventListener('mousemove', click, false);
                
                x = firstCanvas.width;
                y = firstCanvas.height;
                context.drawImage(firstCanvas, 0, 0);
                firstContext.clearRect(0, 0, firstCanvas.width, firstCanvas.height);
    }, false); 

       secondCanvas.addEventListener('mouseup', function() { 
              secondCanvas.removeEventListener('mousemove', line, false);
              secondCanvas.removeEventListener('mousemove', rectangle, false);
              secondCanvas.removeEventListener('mousemove', circle, false);
              secondCanvas.removeEventListener('mousemove', triangle, false);              

                context.drawImage(secondCanvas, 0, 0);
                secondContext.clearRect(0, 0, secondCanvas.width, secondCanvas.height);
    }, false); 
      
      
    for (var i = 0; i < numbers.length; i++){
    $("#" + numbers[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
    for (i = 0; i < pars.length; i++){
    $("#" + pars[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
    for (i = 0; i < dependency.length; i++){
    $("#" + dependency[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
} 
     for (i = 0; i < englishLettersLower.length; i++){
    $("#" + englishLettersLower[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
    for (i = 0; i < englishLettersUpper.length; i++){
    $("#" + englishLettersUpper[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
    for (i = 0; i < primalOperations.length; i++){
    $("#" + primalOperations[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
    for (i = 0; i < write.length; i++){
    $("#" + write[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
for (i = 0; i < greeceLettersLower.length; i++){
    $("#" + greeceLettersLower[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
for (i = 0; i < greeceLettersUpper.length; i++){
    $("#" + greeceLettersUpper[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
for (i = 0; i < sets.length; i++){
    $("#" + sets[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
for (i = 0; i < symbols.length; i++){
    $("#" + symbols[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}
for (i = 0; i < shape.length; i++){
    $("#" + shape[i]).get(0).addEventListener('click', function(e) { 
     Stamp(e.target.id); }, false);
}

    $('#clearCanvas').get(0).addEventListener('click', function(e) { $("#clearCanvas").fadeOut("fast").fadeIn("fast"); clearCanvas(); }, false);
    $('#freeMode').get(0).addEventListener('click', function(e) { switcher = 'free';$('#preciseMode').css("background-color", "white");
    $('#freeMode').fadeOut("fast").fadeIn("fast").css("background-color", "red"); }, false);
    $('#preciseMode').get(0).addEventListener('click', function(e) { switcher = 'precise';$('#preciseMode').fadeOut("fast").fadeIn("fast").css("background-color", "red");
    $('#freeMode').css("background-color", "white"); }, false);
    $('#XXS').get(0).addEventListener('click', function(e) { $('#XXS').fadeOut("fast").fadeIn("fast");sizeX = 16;sizeY = 16; }, false);
    $('#XS').get(0).addEventListener('click', function(e) { $('#XS').fadeOut("fast").fadeIn("fast");sizeX = 24;sizeY = 24; }, false);
    $('#_S').get(0).addEventListener('click', function(e) { $('#_S').fadeOut("fast").fadeIn("fast");sizeX = 32;sizeY = 32; }, false);
    $('#_M').get(0).addEventListener('click', function(e) { $('#_M').fadeOut("fast").fadeIn("fast");sizeX = 48;sizeY = 48; }, false);
    $('#XM').get(0).addEventListener('click', function(e) { $('#XM').fadeOut("fast").fadeIn("fast");sizeX = 64;sizeY = 64; }, false);
    $('#_L').get(0).addEventListener('click', function(e) { $('#_L').fadeOut("fast").fadeIn("fast");sizeX = 96;sizeY = 96; }, false);
    $('#XL').get(0).addEventListener('click', function(e) { $('#XL').fadeOut("fast").fadeIn("fast");sizeX = 128;sizeY = 128; }, false);
    $('#XXL').get(0).addEventListener('click', function(e) { $('#XXL').fadeOut("fast").fadeIn("fast");sizeX = 192;sizeY = 192; }, false);
    $('#FAQ').get(0).addEventListener('click', function(e) { FAQ(); }, false);
    $('#save').get(0).addEventListener('click', function(e) { $("#save").fadeOut("fast").fadeIn("fast");save(); }, false);
}


function click(e) {
  
  if (switcher == 'free'){
        
    if (stampId.length > 0) {
       for (i = 0; i < size.length; i++){
        if (sizeX == size[i]){
       x = e.pageX - shiftX[i];
       y = e.pageY - shiftY[i];  
       sizeX = size[i];
       sizeY = size[i];
        }
       }
    }

function clear() {
 firstContext.clearRect(0, 0, firstCanvas.width, firstCanvas.height);
}
function draw() {
 clear();
 for (var i = 0; i < shape.length; i++){
       if (stampId == '#' + shape[i] + 'Img'){
          currentShape = shape[i];
          return;
       }  
     }
  firstContext.drawImage($(stampId).get(0), x, y, sizeX, sizeY);
}
function initiate() {
 return setInterval(draw, 10);
}
initiate(); 
  }
}  


function Stamp(id) {

    stampId = '#' + id;
     if (lastStampId == stampId){
    stampId = '';
  }
    currentShape = '';
    for (var i = 0; i < shape.length; i++){
       if (stampId == '#' + shape[i] + 'Img'){
          currentShape = shape[i];
       } 
     }
    if (currentShape == 'marker' || currentShape == 'eraser'){
      canvas.style.display = 'block';
      firstCanvas.style.display = 'none';
      secondCanvas.style.display = 'none';          
    }  
    if (currentShape == ''){
      firstCanvas.style.display = 'block';
      secondCanvas.style.display = 'none';          
    }  
     if (currentShape == 'line' || currentShape == 'rectangle' || currentShape == 'triangle' || currentShape == 'circle'){
      secondCanvas.style.display = 'block';          
    }  

      if (switcher == 'precise') {
       
       if (counterWidth >= canvas.width - sizeX){
   counterHeight += 125;
   counterWidth = 1;
   }
      if (counterHeight >= canvas.height - sizeY){
           clearCanvas();       
      }
     for (var i = 0; i < shape.length; i++){
       if (stampId == '#' + shape[i] + 'Img'){
          currentShape = shape[i];
          return;
       }  
     }
     if (stampId.length > 0) {
        firstContext.drawImage($(stampId).get(0), counterWidth, counterHeight, sizeX, sizeY);
        counterWidth+=sizeX;
        context.drawImage(firstCanvas, 0, 0);
        firstContext.clearRect(0, 0, firstCanvas.width, firstCanvas.height);
    }
    }

    $(lastStampId).css("border", "0px double white");

    $(stampId).fadeOut("fast").fadeIn("fast").css("border", "1px double black");
    

    lastStampId = stampId;    
  }

function clearCanvas(){
 context.clearRect(0, 0, canvas.width, canvas.height);
  counterWidth=1; 
   counterHeight=0;
}

function eraser() {
                
                 context.globalCompositeOperation = 'destination-out';
                 context.lineTo(x, y);
                 context.stroke();
}


function marker() {

                 context.lineTo(x, y);
                 context.stroke();
}

function rectangle() {
    secondContext.clearRect(0, 0, secondCanvas.width, secondCanvas.height);
                
                var xx = Math.min(x, startX);
                var yy = Math.min(y, startY);
                var width = Math.abs(x - startX);
                var height = Math.abs(y - startY);
                secondContext.strokeRect(xx, yy, width, height);
}


function triangle() {
    secondContext.clearRect(0, 0, secondCanvas.width, secondCanvas.height);
                
                secondContext.beginPath();
                secondContext.moveTo(startX, startY);
                secondContext.lineTo(x, y);
                secondContext.moveTo(x, y);
                secondContext.lineTo(startX, y);
                secondContext.moveTo(startX, y);
                secondContext.lineTo(startX, startY);
                secondContext.stroke();
                secondContext.closePath();
}


function line() {
       secondContext.clearRect(0, 0, secondCanvas.width, secondCanvas.height);
    
                secondContext.beginPath();
                secondContext.moveTo(startX, startY);
                secondContext.lineTo(x, y);
                secondContext.stroke();
                secondContext.closePath();
}


function circle() {
    secondContext.clearRect(0, 0, secondCanvas.width, secondCanvas.height);

    var xx = (x + startX) / 2;
    var yy = (y + startY) / 2;

    var radius = Math.max(
      Math.abs(x - startX),
      Math.abs(y - startY)) / 2;

    secondContext.beginPath();
    secondContext.arc(xx, yy, radius, 0, Math.PI * 2, false);
    secondContext.stroke();
    secondContext.closePath();
}


function save() {
canvas.style.display = 'block';
firstCanvas.style.display = 'none';
secondCanvas.style.display = 'none';
}


function FAQ() {
 alert("Welcome to the Graphic math formulas photoshop." + '\n' +
"My name is Artem Kordysh and I'm the author." + '\n' +
"How to use the editor?" + '\n' + "In free mode you can paint anywhere you like:" + '\n' +
"just press " + "'Free mode', " + "choose desired symbol, "+ '\n' +
"and click on the canvas where you want the symbol to be."+ '\n' +
"In precise mode you can build formulas only in precise sequence." + '\n' +
"To start you need to press " + "'Precise mode', " + "choose desired symbol, " + 
"and you'll have your symbol on the canvas."+ '\n' +
"You can also paint some geometric figures and use marker." + '\n' +
"To erase some parts of the drawing - choose eraser button." + '\n' +
"To clear all canvas - choose trash button."+ '\n' +
"You can choose the size too."+ '\n' +
"To make symbol bold in free mode you have to click some times."+ '\n' +
"To save the image - enter the save mode by pressing save button," + '\n' +
"right click on the canvas and choose 'save image as'." + '\n' +
"Don't forgete to exit save mode! You can do it by selecting any icon again." + '\n' +
"How to invoke the FAQ you have already guessed, I suppose." + '\n' +
"Have a nice drawing!");
}