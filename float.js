var flipArray = ['size', 'numbers', 'englishLettersLower', 'englishLettersLower2', 
 'englishLettersUpper', 'englishLettersUpper2', 'greeceLettersLower', 'greeceLettersLower2', 
 'greeceLettersUpper', 'greeceLettersUpper2', 'primalOperations', 'pars',
 'dependency', 'symbols', 'symbols2', 'sets', 'shape', 'write'];


$(document).ready(function() {
$(".flipNumbers").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".numbers").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipSize").click(function() {
$(".size").slideToggle("slow");
});
});

$(document).ready(function() {
$(".flipEnglishLettersLower").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'englishLettersLower2' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".englishLettersLower").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipEnglishLettersLower").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'englishLettersLower' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".englishLettersLower2").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipEnglishLettersUpper").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'englishLettersUpper' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".englishLettersUpper2").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipEnglishLettersUpper").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'englishLettersUpper2' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".englishLettersUpper").fadeToggle("fast");
});
});


$(document).ready(function() {
$(".flipGreeceLettersLower").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'greeceLettersLower2' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".greeceLettersLower").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipGreeceLettersLower").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'greeceLettersLower' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".greeceLettersLower2").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipGreeceLettersUpper").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'greeceLettersUpper2' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".greeceLettersUpper").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipGreeceLettersUpper").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'greeceLettersUpper' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".greeceLettersUpper2").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipPrimalOperations").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".primalOperations").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipDependency").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".dependency").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipPars").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".pars").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipWrite").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".write").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipShape").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".shape").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipSets").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".sets").fadeToggle("fast");
});
});

$(document).ready(function() {
$(".flipSymbols").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'symbols2' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".symbols").fadeToggle("fast");
});
});


$(document).ready(function() {
$(".flipSymbols").click(function() {
for (var i = 0; i < flipArray.length; i++){
	if (flipArray[i] == 'symbols' || flipArray[i] == 'size'){
		continue;
	}
$("." + flipArray[i]).fadeOut("fast");
}
$(".symbols2").fadeToggle("fast");
});
});

